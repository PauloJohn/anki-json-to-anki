## A small script to convert a json file to an anki deck

### Installing requirements:
```
pip install -r requirements.txt
```

### running script:

```
./anki_from_json.py yourfile.json DeckName
```


I assume that the json is and array of objs like that:
```json
[
 {
    "front": "Answer...",
    "back": "Question..."
  },
  {
    "front": "Answer",
    "back": "Question..."
  }
]
```

obs: I do not intent to update this script and I made just because I needed it.
