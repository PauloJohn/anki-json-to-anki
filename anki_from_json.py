#!/usr/env python

import genanki
import json
import sys
from deck_model import my_model

json_file = sys.argv[1]
deck_name = sys.argv[2]

with open(json_file, 'r') as f:
    raw_deck = json.load(f)

my_deck = genanki.Deck(
    1724897887,
    deck_name)

for card in raw_deck:
    aNote = genanki.Note(
        model=my_model, fields=[card.get('front'), card.get('back')]
    )
    my_deck.add_note(aNote)


genanki.Package(my_deck).write_to_file(
    './{}.apkg'.format(deck_name))
